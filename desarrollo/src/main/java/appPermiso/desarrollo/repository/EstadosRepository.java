package appPermiso.desarrollo.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import appPermiso.desarrollo.entitys.Estados;
public interface EstadosRepository  extends JpaRepository <Estados, Integer>{
    
}
