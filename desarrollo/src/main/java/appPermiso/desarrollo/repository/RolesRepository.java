package appPermiso.desarrollo.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import appPermiso.desarrollo.entitys.*;
public interface RolesRepository extends JpaRepository <Roles, Integer>{
    
}
