package appPermiso.desarrollo.entitys;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="generalPermisos")

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Permisos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "tipo_id")
    private TipoPermisos tipoPermiso;

    @NotNull
    private Date fechaInicio;

    @NotNull
    private Date fechaFin;

    private String descripcion;

    @ManyToOne
    @JoinColumn(name = "estado_id")
    private Estados estadoPermiso;
    
    private Date fechaSolicitud;

    private Date fechaAprobacion;

    @ManyToOne
    @JoinColumn(name = "aprobado_por")
    private Usuario aprobadoPor;
    
}
