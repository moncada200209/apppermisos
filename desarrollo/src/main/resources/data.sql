INSERT INTO estado_Gerencia (nombre) VALUES ('Aprobado');
INSERT INTO estado_Gerencia (nombre) VALUES ('Desaprobado');

INSERT INTO roles_Usuario (nombre) VALUES ('Admin');
INSERT INTO roles_Usuario (nombre) VALUES ('Gerente');
INSERT INTO roles_Usuario (nombre) VALUES ('Colaborador');

INSERT INTO permisos_Disponibles (nombre) VALUES ('Vacaciones');
INSERT INTO permisos_Disponibles (nombre) VALUES ('Permiso');
INSERT INTO permisos_Disponibles (nombre) VALUES ('Licencia Maternidad');
